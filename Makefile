OBSD_SITE = "https://gitlab.com/DIOBS/diobs.gitlab.io/-/jobs/artifacts/main/download?job=pages"
ACH_SITE = "https://gitlab.com/DIOBS/atlas_capital_humano/-/jobs/artifacts/main/download?job=pages"

.PHONY: sync-website sync-app sync test up down

# Sync static websites
sync-website:
	@curl --output artifacts.zip --location "$(OBSD_SITE)"
	@unzip artifacts.zip
	@mv public site
	@rm artifacts.zip
	@curl --output artifacts.zip --location "$(ACH_SITE)"
	@unzip artifacts.zip
	@mv public site/atlas_capital_humano
	@rm artifacts.zip

sync-app:
	@[[ -d "ShinyApps" ]] || mkdir ShinyApps
	@[[ -d "ShinyApps/atlasCapitalHumano" ]] || git clone https://gitlab.com/DIOBS/atlasCapitalHumano.git ShinyApps/atlasCapitalHumano

sync: sync-website sync-app

test:
	@docker-compose up --build

up:
	@docker-compose up --build --detach

down:
	@docker-compose down

clean:
	@docker volume rm $(docker volume ls -q | grep '^diobs')


