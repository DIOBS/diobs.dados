# diobs.dados

*Backend* da página do [Observatório de Dados](https://observatoriodefortaleza.fortaleza.ce.gov.br/dados/) do [Observatório de Fortaleza](https://observatoriodefortaleza.fortaleza.ce.gov.br/).

lembre-se de criar as credenciais do banco de dados no arquivo =.env=
